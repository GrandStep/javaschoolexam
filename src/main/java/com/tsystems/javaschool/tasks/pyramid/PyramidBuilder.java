package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given inputNumbers
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int height = getHeight(inputNumbers.size());
        int base = height * 2 - 1;
        int k = 0;
        int[][] pyramid = new int[height][base];

        if(inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        List<Integer> input = new ArrayList<>(inputNumbers);

        Collections.sort(input);

        for (int i = height - 1; i >= 0; i--) {
            for (int j = base - 1; j >= k; j = j - 2) {
                pyramid[i][j] = input.remove(input.size() - 1);
            }
            base--;
            k++;
        }
        return pyramid;
    }

    public int getHeight(int size) throws CannotBuildPyramidException {
        if (size > 2) {
            double x = (Math.pow(size * 8 + 1, 0.5) - 1) / 2;
            if (x % 1 == 0.)
                return (int) x;
        }
        throw new CannotBuildPyramidException();
    }


}
